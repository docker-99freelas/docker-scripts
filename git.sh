#!/bin/sh

set -e

docker run -ti --rm --user $(id -u):$(id -g) -v ${HOME}:/root -v $(pwd):/git 99freelas/git "$@"
